# McCormick Chili

### Ingredients
 * 1lb Lean Ground Beef
 * 2x 28oz Petite Diced Tomatoes Cans
 * 2x 15.5oz Light Red Kidney Beans (NO Salt Added) Cans
 * 2x McCormick Hot Chili Spice Mix
### Instructions
 * Small amount of water in a large pot.
 * Cook the ground beef thoroughly
 * Drain ground beef and water through a strainer
 * Put the ground beef back in the pot
 * Season ground beef with lots of black pepper + spices as desired
 * Dump all the canned ingredients + the McCormick spice mix in the pot, mix.
 * Bring to a boil and stir. Boil for 10 minutes.
 * Serve with garlic or cornbread as desired.
